class UberPoll extends Car {
    String brand;
    String model;

    public UberPoll(String license, Acount driver, String brand, String model){
        super(license, driver);
        this.brand = brand;
        this.model = model;
    }
}
