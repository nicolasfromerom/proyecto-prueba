class UberX extends Car {
    String brand;
    String model;

    public UberX(String license, Acount driver, String brand, String model){
        super(license, driver);
        this.brand = brand;
        this.model = model;
    }
}
