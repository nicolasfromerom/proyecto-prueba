class Car {
    Integer id;
    String license;
    Acount driver;
    Integer passenger;

    public Car(String license, Acount driver){
        this.license = license;
        this.driver = driver;
    }

    void prinDataCar()
    {
        System.out.println("Licence: " + license + " Drive: " + driver.name);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Acount getDriver() {
        return driver;
    }

    public void setDriver(Acount driver) {
        this.driver = driver;
    }

    public Integer getPassenger() {
        return passenger;
    }

    public void setPassenger(Integer passenger) {
        this.passenger = passenger;
    }

}
